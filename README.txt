Build Instructions:
- No compiling needed. Just open with any web browser (Chrome preferred to
  obtain the drop-down calendar).
- If deploying live: place all files in the relevant backend WEB-INF folder.

Code Ownership:
dashboard.html                      Leo Lung (JavaScript), Vincent Liu (HTML)
images/                             Vincent Liu
map.html                            Dennis Liang
styles.css                          Vincent Liu
welcome.html                        Leo Lung (JavaScript), Vincent Liu (HTML)